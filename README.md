# FPGA-misc

Este repositorio contiene codigo fuente en VHDL/Verilog desde distintos IP Cores de prueba, junto con sus testbenchs y proyectos de implementación. Ademas se disponen de distintas tools a instalar para poder trabajar con ellos.

## Directorios

El proyecto se encuentra divido en distintos directorios con funciones específicas:

```
├── Readme.md
├── src
│   ├── misc
│   │   └── freq_divider.vhdl
│   └── pwm
│       ├── pwm.v
│       └── pwm.vhdl
├── syn
│   └── adc_pwm
│       ├── adcpwm.cdf
│       ├── adc_pwm.sdc
│       ├── adc_pwm.vhdl
│       ├── Manifest.py
│       ├── module.tcl
│       └── pinout.tcl
├── test
│   └── pwm_tb
│       ├── Makefile
│       ├── pwm_tb.py
└── utils
    └── docker
        ├── buildimage.sh
        ├── Dockerfile
        └── dockershell.sh
```

### src (Sources)

Este directorio contiene archivos fuente, en su mayoría van a ser archivos .vhdl o .v (Verilog). Tambien puede existir en las mismas algún archivo python que se use para describir un hardware con myhdl.

Es recomendable que los fuentes se agrupen por core en un directorio particular con un readme si es necesario. Por ejemplo si se trata de IP core que es un timer con interfaz AXI la organización del directorio podría ser:

```
axi_timer          (nombre del directorio coincidente con el top level)
├── Readme.md      (Readme que explica que tiene el IP Core)
├── axi_slave.vhdl (wrapper axi slave)
├── axi_timer.vhdl (top level)
└── timer.vhdl     (core encargado de generar el timer)
```

Puede existir tambien un directorio `misc` que contenga cores misceláneos.


### syn (Synthesis projects)

Acá van los proyectos de síntesis sobre cores determinados. Al igual que sources es importante que el nombre del directorio de cada proyecto se identifique con su función interna (mismo nombre que el top level generalmente).

Si se está utilizando HDLMake se deben incluir archivos como:

```
axi_timer
├── Readme.md                 (Readme que explica como debe correrse el buildeo)
├── axi_timer.cdf             (archivo de programación por jtag Altera)
├── axi_timer_top_level.vhdl  (top level con descripción de lo que va a grabarse)
├── Manifest.py               (Manifest de HDLMake, contiene los path a los sources y config)
├── module.tcl                (archivo base para setear uso de quartus)
└── pinout.tcl                (pinout de conexión de las señales a los pines especificos de la FPGA)
```

### test (Testbenches)

Acá se encuentran todos los testbenches escritos en COCOTB para los distintos IP Cores y en sus distintas configuraciones. Es importante que el nombre del directorio principal sea descriptivo para saber que se está testeando ahi dentro.

```
axi_timer_tb
├── Readme.md             (Readme que explica como debe correrse el test)
├── Makefile              (Makefile de configuración de cocotb, con sources y tipo de simulacion)
└── axi_timer_tb.py       (Python con clases y test del ip core)
```

### utils

Acá vamos a poner todos los scripts, herramientas y cosas utiles comunes a todos los proyectos, pero no fuentes de proyectos.


## Setup del sistema

Para comenzar vamos a tener que instalarnos docker en el caso de linux debian stretch es:

```
    # sudo apt-get install apt-transport-https dirmngr
    # sudo echo 'deb https://apt.dockerproject.org/repo debian-stretch main' >> /etc/apt/sources.list
    # sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    # sudo apt-get update
    # sudo apt-get install docker-engine
    # sudo usermod -a -G docker $USER
```

O sus variantes según el O.S de linux que tengamos en el host.

En el caso de usar windows, deberán descargarse el instalador de docker para windows de: https://docs.docker.com/docker-for-windows/install/

Una vez instalado docker, se deberá correr el build de la imagen

`./utils/docker/buildimage.sh`

En el caso de que en windows no se pueda correr el bash se puede correr el comando que está dentro que es:

`docker build -t pes_image Dockerfile `

Este comando buildea e instala en el host la imagen del sistema armado con todas las tools para trabajar acá

Luego corriendo el script de dockershell se ingresa a la consola del sistema buildeado en docker.

`./dockershell.sh`

Una vez dentro seguir el readme de cada lugar