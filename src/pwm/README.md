# PWM Core

Este es un core simple que se encarga de generar una señal PWM de resolución ajustable por generics y que cuyo valor se carga por registro. 

## Interfaces y explicación de cada una

El core se compone de las siguientes entradas y salidas

* clk_i: **clock input** señal de clock de referencia del ip core
* rst_i: **reset input** señal de reset sincronico al clock y de acción por flanco ascendente.
* ena_i: **enable input** señal de enable. Si está en estado bajo se congela el proceso del core, manteniendo los estados. En estado alto deja operar al core.
* data_i: **data input** Por acá se carga el ciclo de actividad. El ancho de esta señal es configurable por generics del IP Core (W_COUNT).
* sample_rate_o: ** Sample Rate Input** está señal define cada cuanto se actualiza el ciclo de actividad en la maquina de estados interna del IP Core. Si el sample rate no es un multiplo entero de la resolución del PWM definida por **W_COUNT** entonces la actualización de los valores podría ser problematica y causar pequeños glitches o transitorios. Es recomendable trabajar en multiplos de la misma.
* data_o: **data ouput** Señal PWM de salida.

## Lenguajes

El core está escrito en VHDL y Verilog teniendo el mismo funcionamiento base.

