//////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 David Caruso <carusodvd@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////
// Description
// 
// PWM generator
//////////////////////////////////////////////////////////////////////////
// Author: David Caruso <carusodvd@gmail.com>
//////////////////////////////////////////////////////////////////////////
`timescale 1ns/1ps

module pwm #(
    parameter W_COUNT = 8
) (
    input                            clk_i,
    input                            rst_i,
    input                            ena_i,
    input                            sample_rate_i,
    input           [W_COUNT-1:0]    data_i,
    output reg                       data_o
);
 

reg [W_COUNT-1:0] duty_r;
reg [W_COUNT-1:0] counter_r;

// DUTY_LATCH
always @(posedge clk_i) begin
    if (rst_i)
        duty_r <= 0;
    else if (sample_rate_i & ena_i)
        duty_r <= data_i;
end
    
// PWM_MODULATOR
always @(posedge clk_i) begin
    if (rst_i) begin
        counter_r <= 0;
        data_o <= 1'b0;
    end
    else if (ena_i) begin
        if (counter_r == 0)
            data_o <= 1'b1;
        if (counter_r >= duty_r)
            data_o <= 1'b0;
        counter_r <= counter_r +1;
    end
end

`ifdef COCOTB_SIM
    initial begin
        $dumpfile("sim_build/pwm_tb.vcd");
        $dumpvars (0,pwm);
    end
`endif

endmodule