# PWM testbench

El testbench es un único test que primero valida los estados de reset del ip core y luego carga un duty cycle. Para medirlo toma un período completo de la señal y calcula el valor medio, si coincide con lo pedido da por válido el test.

# Cómo correrlo?

Como el core está escrito en VHDL y en Verilog se puede simular cualquiera de los dos, por lo que en el makefile se elige cual se quiere correr cambiando la variable de entorno `TOPLEVEL_LANG` desde consola en dos opciones (muy faciles de recordar):

	export TOPLEVEL_LANG=vhdl    # Corre el test para el core en VHDL
	export TOPLEVEL_LANG=verilog # Corre el test para el core en Verilog

Luego en la consola y ya dentro de docker se corre: 

	make

y ya con eso debería correr el testbench, mostrando en consola lo siguiente:

```
make results.xml
make[1]: Entering directory '/home/david/FPGA/UTN/test/pwm_tb'
cd sim_build && /usr/local/bin/ghdl -a  --work=work /home/david/FPGA/UTN/test/pwm_tb/../../src/pwm/pwm.vhdl && /usr/local/bin/ghdl -e  --work=work pwm
mkdir -p /cocotb/build/obj/x86_64
mkdir -p /cocotb/build/libs/x86_64
make -C /cocotb/lib/utils SIM=ghdl
make[2]: Entering directory '/cocotb/lib/utils'
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DGHDL -Wstrict-prototypes -Waggregate-return -DGHDL -c -I/cocotb/include -I/usr/include/python2.7  -o /cocotb/build/obj/x86_64/cocotb_utils.o cocotb_utils.c
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DGHDL -Wstrict-prototypes -Waggregate-return -DGHDL -shared -Xlinker -L/usr/lib -o /cocotb/build/libs/x86_64/libcocotbutils.so /cocotb/build/obj/x86_64/cocotb_utils.o    
make[2]: Leaving directory '/cocotb/lib/utils'
make -C /cocotb/lib/gpi_log SIM=ghdl
make[2]: Entering directory '/cocotb/lib/gpi_log'
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DGHDL -Wstrict-prototypes -Waggregate-return -DFILTER -DGHDL -c -I/cocotb/include -I/usr/include/python2.7  -o /cocotb/build/obj/x86_64/gpi_logging.o gpi_logging.c
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DGHDL -Wstrict-prototypes -Waggregate-return -DFILTER -DGHDL -shared -Xlinker -L/usr/lib -o /cocotb/build/libs/x86_64/libgpilog.so /cocotb/build/obj/x86_64/gpi_logging.o   -lpython2.7 -lpthread -ldl  -lutil -lm  
make[2]: Leaving directory '/cocotb/lib/gpi_log'
make -C /cocotb/lib/embed SIM=ghdl
make[2]: Entering directory '/cocotb/lib/embed'
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DGHDL -Wstrict-prototypes -Waggregate-return -DPYTHON_SO_LIB=libpython2.7.so -DGHDL -c -I/cocotb/include -I/usr/include/python2.7  -o /cocotb/build/obj/x86_64/gpi_embed.o gpi_embed.c
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DGHDL -Wstrict-prototypes -Waggregate-return -DPYTHON_SO_LIB=libpython2.7.so -DGHDL -shared -Xlinker -L/usr/lib -o /cocotb/build/libs/x86_64/libcocotb.so /cocotb/build/obj/x86_64/gpi_embed.o   -lpython2.7 -lpthread -ldl  -lutil -lm  -lgpilog -lcocotbutils -L/cocotb/build/libs/x86_64
make[2]: Leaving directory '/cocotb/lib/embed'
make -C /cocotb/lib/gpi EXTRA_LIBS= EXTRA_LIBDIRS= SIM=ghdl
make[2]: Entering directory '/cocotb/lib/gpi'
g++ -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DVPI_CHECKING -DLIB_EXT=so -DSINGLETON_HANDLES -DGHDL -c -I/cocotb/include -I/usr/include/python2.7  -o /cocotb/build/obj/x86_64/GpiCbHdl.o GpiCbHdl.cpp
g++ -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DVPI_CHECKING -DLIB_EXT=so -DSINGLETON_HANDLES -DGHDL -c -I/cocotb/include -I/usr/include/python2.7  -o /cocotb/build/obj/x86_64/GpiCommon.o GpiCommon.cpp
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DVPI_CHECKING -DLIB_EXT=so -DSINGLETON_HANDLES -DGHDL -Wstrict-prototypes -Waggregate-return -DGHDL -shared -Xlinker -L/usr/lib -o /cocotb/build/libs/x86_64/libgpi.so  /cocotb/build/obj/x86_64/GpiCbHdl.o /cocotb/build/obj/x86_64/GpiCommon.o  -lcocotbutils -lgpilog -lcocotb -lstdc++ -L/cocotb/build/libs/x86_64
make[2]: Leaving directory '/cocotb/lib/gpi'
make -C /cocotb/lib/simulator SIM=ghdl
make[2]: Entering directory '/cocotb/lib/simulator'
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DGHDL -Wstrict-prototypes -Waggregate-return  -DGHDL -c -I/cocotb/include -I/usr/include/python2.7  -o /cocotb/build/obj/x86_64/simulatormodule.o simulatormodule.c
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DGHDL -Wstrict-prototypes -Waggregate-return  -DGHDL -shared -Xlinker -L/usr/lib -o /cocotb/build/libs/x86_64/libsim.so /cocotb/build/obj/x86_64/simulatormodule.o   -lgpi -lgpilog -lpython2.7 -lpthread -ldl  -lutil -lm  -L/cocotb/build/libs/x86_64
ln -sf libsim.so /cocotb/build/libs/x86_64/simulator.so
make[2]: Leaving directory '/cocotb/lib/simulator'
make -C /cocotb/lib/vpi EXTRA_LIBS= EXTRA_LIBDIRS= SIM=ghdl
make[2]: Entering directory '/cocotb/lib/vpi'
g++ -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DVPI_CHECKING -DGHDL -c -I/cocotb/include -I/usr/include/python2.7  -o /cocotb/build/obj/x86_64/VpiImpl.o VpiImpl.cpp
g++ -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DVPI_CHECKING -DGHDL -c -I/cocotb/include -I/usr/include/python2.7  -o /cocotb/build/obj/x86_64/VpiCbHdl.o VpiCbHdl.cpp
gcc -Werror -Wcast-qual -Wcast-align -Wwrite-strings -Wall -Wno-unused-parameter -fno-common -g -DDEBUG -fpic -DVPI_CHECKING -DGHDL -Wstrict-prototypes -Waggregate-return -DGHDL -shared -Xlinker -L/usr/lib -o /cocotb/build/libs/x86_64/libvpi.so  /cocotb/build/obj/x86_64/VpiImpl.o /cocotb/build/obj/x86_64/VpiCbHdl.o   -lgpilog -lgpi -lstdc++  -L/cocotb/build/libs/x86_64
ln -sf libvpi.so /cocotb/build/libs/x86_64/gpivpi.vpl
ln -sf libvpi.so /cocotb/build/libs/x86_64/cocotb.vpi
make[2]: Leaving directory '/cocotb/lib/vpi'
cd sim_build; \
PYTHONPATH=/cocotb/build/libs/x86_64:/cocotb:/home/david/FPGA/UTN/test/pwm_tb:/home/david/FPGA/UTN/test/pwm_tb/../model:/home/david/FPGA/UTN/test/pwm_tb/../utils: LD_LIBRARY_PATH=/cocotb/build/libs/x86_64:/usr/local/lib:/usr/lib:/usr/lib:/usr/lib:/usr/lib MODULE=pwm_tb \
        TESTCASE= TOPLEVEL=pwm TOPLEVEL_LANG=vhdl \
/usr/local/bin/ghdl -r  pwm --vpi=/cocotb/build/libs/x86_64/libvpi.so --wave=pwm_tb.ghw --ieee-asserts=disable-at-0
loading VPI module '/cocotb/build/libs/x86_64/libvpi.so'
     -.--ns INFO     cocotb.gpi                                GpiCommon.cpp:91   in gpi_print_registered_impl       VPI registered
VPI module loaded!
  0.00ns INFO     cocotb.gpi                                  gpi_embed.c:248  in embed_sim_init                  Running on GHDL version 0.1
  0.00ns INFO     cocotb.gpi                                  gpi_embed.c:249  in embed_sim_init                  Python interpreter initialised and cocotb loaded!
  0.00ns INFO     cocotb                                      __init__.py:115  in _initialise_testbench           Running tests with Cocotb v1.0 from /cocotb
  0.00ns INFO     cocotb                                      __init__.py:131  in _initialise_testbench           Seeding Python random module with 1524345840
  0.00ns INFO     cocotb.regression                         regression.py:167  in initialise                      Found test pwm_tb.normal_test
  0.00ns INFO     cocotb.regression                         regression.py:287  in execute                         Running test 1/1: normal_test
  0.00ns INFO     ..outine.normal_test.0x7f3ead3bc910       decorators.py:191  in send                            Starting test: "normal_test"
                                                                                                                  Description: None
  0.00ns INFO     cocotb.pwm                                    pwm_tb.py:72   in normal_test                     > Starting Normal Test
 90.00ns INFO     cocotb.pwm                                    pwm_tb.py:49   in signal_reconstructor            > Start reconstructor
7780.00ns INFO     cocotb.pwm                                    pwm_tb.py:99   in normal_test                     > Mean: 80
7780.00ns INFO     cocotb.regression                         regression.py:223  in handle_result                   Test Passed: normal_test
7780.00ns INFO     cocotb.regression                         regression.py:176  in tear_down                       Passed 1 tests (0 skipped)
7780.00ns INFO     cocotb.regression                         regression.py:341  in _log_test_summary               ****************************************************************************
                                                                                                                   ** TEST                PASS/FAIL  SIM TIME(NS)  REAL TIME(S)  RATIO(NS/S) **
                                                                                                                   ****************************************************************************
                                                                                                                   ** pwm_tb.normal_test    PASS         7780.00          0.12     67547.01  **
                                                                                                                   ****************************************************************************
                                                                                                                   
7780.00ns INFO     cocotb.regression                         regression.py:358  in _log_sim_summary                *************************************************************************************
                                                                                                                   **                                 ERRORS : 0                                      **
                                                                                                                   *************************************************************************************
                                                                                                                   **                               SIM TIME : 7780.00 NS                             **
                                                                                                                   **                              REAL TIME : 0.21 S                                 **
                                                                                                                   **                        SIM / REAL TIME : 37878.99 NS/S                          **
                                                                                                                   *************************************************************************************
                                                                                                                   
7780.00ns INFO     cocotb.regression                         regression.py:184  in tear_down                       Shutting down...
make[1]: Leaving directory '/home/david/FPGA/UTN/test/pwm_tb'
```
