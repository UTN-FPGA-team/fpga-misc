########################################################################
# Copyright (c) 2017 David Caruso <carusodvd@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
########################################################################
# Description
# 
# Testbench for dac with COCOTB!
########################################################################
# Author: David Caruso <carusodvd@gmail.com>
########################################################################

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
import numpy as np

class PWM_Ctrl:
    def __init__(self, dut):
        self.dut = dut
        self.dut.data_i = 0
        self.dut.rst_i = 1
        self.dut.ena_i = 0
        self.dut.sample_rate_i=0
        self.signal_out = []

    @cocotb.coroutine
    def reset(self):
        self.dut.rst_i = 1
        for i in range(10):
            yield RisingEdge(self.dut.clk_i)
        self.dut.rst_i = 0

    @cocotb.coroutine
    def signal_reconstructor(self, bits):
        self.dut._log.info("> Start reconstructor")
        while ((self.dut.sample_rate_i.value.integer ==0) | (self.dut.ena_i.value.integer == 0)): # espera el primer sincronismo de carga de duty
            yield RisingEdge(self.dut.clk_i)
        while True:
            yield RisingEdge(self.dut.clk_i)
            self.signal_out.append(self.dut.data_o.value.integer*(2**bits))

    def get_signal(self):
        return self.signal_out

    @cocotb.coroutine
    def sample_rate_gen(self, rate):
        while True:
            self.dut.sample_rate_i = 1
            yield RisingEdge(self.dut.clk_i)
            self.dut.sample_rate_i = 0
            for i in range(int(rate-1)):
                yield RisingEdge(self.dut.clk_i)

# def check_mean_value(dut, value):

@cocotb.test()
def normal_test(dut):
    dut._log.info("> Starting Normal Test")
    cocotb.fork(Clock(dut.clk_i,10,units='ns').start())
    bits = 8
    size = 2**bits

    pwm_dev = PWM_Ctrl(dut)

    yield pwm_dev.reset()

    if dut.data_o.value.integer != 0:
        raise TestFailure("Data output is not 0 after reset")

    cocotb.fork(pwm_dev.sample_rate_gen(size))

    cocotb.fork(pwm_dev.signal_reconstructor(bits))
    
    dut.data_i = 80
    yield RisingEdge(dut.clk_i)
    dut.ena_i = 1
    
    for n in range(size*3):
        yield RisingEdge(dut.clk_i)

    signal_dut = pwm_dev.get_signal()

    signal_mean=int(np.mean(signal_dut[0:size*2]))

    dut._log.info("> Mean: {}".format(signal_mean))

    if dut.data_i.value != int(signal_mean):
        raise TestFailure("PWM output and signal mean doesn't match: mean={}, request={}".format(signal_mean, dut.data_i.value.integer))